package com.sudox.events

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EventEmitterTest : Assert() {

    private var eventEmitter: EventEmitter? = null

    @Before
    fun setUp() {
        eventEmitter = EventEmitter()
    }

    @Test
    fun testSanityWhenDataNull() {
        var received = false

        eventEmitter!!.on("event") { received = true }
        eventEmitter!!.emit("event")

        assertTrue(received)
    }

    @Test
    fun testSanityWhenDataNotNull() {
        var message: String? = null

        eventEmitter!!.on("event") { message = it as String }
        eventEmitter!!.emit("event", "Hello")

        assertEquals("Hello", message)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun testSanityWhenUsingVararg() {
        var message: Array<Any>? = null
        val valid = arrayOfNulls<String>(2).apply {
            this[0] = "Hello"
            this[1] = "World"
        }

        eventEmitter!!.on("event") { message = it as Array<Any> }
        eventEmitter!!.emit("event", "Hello", "World")

        assertArrayEquals(valid, message)
    }

    @Test
    fun testMultipleEmittingOnceCallback() {
        var message: String? = null
        var count = 0

        eventEmitter!!.once("event") { message = it as String; count++ }
        eventEmitter!!.emit("event", "Hello")
        eventEmitter!!.emit("event", "World!")

        assertEquals(1, count)
        assertEquals("Hello", message)
    }

    @Test
    fun testMultipleEmittingOnCallback() {
        var message: String? = null
        var count = 0

        eventEmitter!!.on("event") { message = it as String; count++ }
        eventEmitter!!.emit("event", "Hello")
        eventEmitter!!.emit("event", "World!")

        assertEquals(2, count)
        assertEquals("World!", message)
    }

    @Test
    fun testMultipleEmitting() {
        var firstMessage: String? = null
        var secondMessage: String? = null
        var firstCount = 0
        var secondCount = 0

        eventEmitter!!.once("event") { firstMessage = it as String; firstCount++ }
        eventEmitter!!.on("event") { secondMessage = it as String; secondCount++ }
        eventEmitter!!.emit("event", "Hello")
        eventEmitter!!.emit("event", "World!")

        assertEquals(1, firstCount)
        assertEquals(2, secondCount)
        assertEquals("Hello", firstMessage)
        assertEquals("World!", secondMessage)
    }

    @Test
    fun testCallbacksDisablingSanity() {
        var count = 0

        eventEmitter!!.on("event") { count++ }
        eventEmitter!!.once("event") { count++ }
        eventEmitter!!.off("event")
        eventEmitter!!.emit("event", "Hello")

        assertEquals(0, count)
    }

    @Test
    fun testSpecificCallbackDisabling() {
        var count = 0
        val callback: (Any?) -> (Unit) = { count++ }

        eventEmitter!!.on("event", callback)
        eventEmitter!!.once("event", callback)
        eventEmitter!!.off("event", callback)
        eventEmitter!!.emit("event", "Hello")

        assertEquals(0, count)
    }
}