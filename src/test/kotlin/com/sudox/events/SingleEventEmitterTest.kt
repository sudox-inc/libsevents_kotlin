package com.sudox.events

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SingleEventEmitterTest : Assert() {

    private var eventEmitter: SingleEventEmitter? = null

    @Before
    fun setUp() {
        eventEmitter = SingleEventEmitter()
    }

    @Test
    fun testSanityWhenDataNull() {
        var received = false

        eventEmitter!!.on { received = true }
        eventEmitter!!.emit(null)

        assertTrue(received)
    }

    @Test
    fun testSanityWhenDataNotNull() {
        var message: String? = null

        eventEmitter!!.on { message = it as String }
        eventEmitter!!.emit("Hello")

        assertEquals("Hello", message)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun testSanityWhenUsingVararg() {
        var message: Array<Any>? = null
        val valid = arrayOfNulls<String>(2).apply {
            this[0] = "Hello"
            this[1] = "World"
        }

        eventEmitter!!.on { message = it as Array<Any> }
        eventEmitter!!.emit("Hello", "World")

        assertArrayEquals(valid, message)
    }

    @Test
    fun testMultipleEmittingOnceCallback() {
        var message: String? = null
        var count = 0

        eventEmitter!!.once { message = it as String; count++ }
        eventEmitter!!.emit("Hello")
        eventEmitter!!.emit("World!")

        assertEquals(1, count)
        assertEquals("Hello", message)
    }

    @Test
    fun testMultipleEmittingOnCallback() {
        var message: String? = null
        var count = 0

        eventEmitter!!.on { message = it as String; count++ }
        eventEmitter!!.emit("Hello")
        eventEmitter!!.emit("World!")

        assertEquals(2, count)
        assertEquals("World!", message)
    }

    @Test
    fun testMultipleEmitting() {
        var firstMessage: String? = null
        var secondMessage: String? = null
        var firstCount = 0
        var secondCount = 0

        eventEmitter!!.once { firstMessage = it as String; firstCount++ }
        eventEmitter!!.on { secondMessage = it as String; secondCount++ }
        eventEmitter!!.emit("Hello")
        eventEmitter!!.emit("World!")

        assertEquals(1, firstCount)
        assertEquals(2, secondCount)
        assertEquals("Hello", firstMessage)
        assertEquals("World!", secondMessage)
    }

    @Test
    fun testCallbacksDisablingSanity() {
        var count = 0

        eventEmitter!!.on { count++ }
        eventEmitter!!.once { count++ }
        eventEmitter!!.off()
        eventEmitter!!.emit("Hello")

        assertEquals(0, count)
    }

    @Test
    fun testSpecificCallbackDisabling() {
        var count = 0
        val callback: (Any?) -> (Unit) = { count++ }

        eventEmitter!!.on(callback)
        eventEmitter!!.once(callback)
        eventEmitter!!.off(callback)
        eventEmitter!!.emit("Hello")

        assertEquals(0, count)
    }
}