package com.sudox.events.structs

import org.junit.Assert
import org.junit.Test

class MultiHashMapTest : Assert() {

    @Test
    fun testInserting() {
        val map = MultiHashMap<String, String>()

        map.insert("names", "Maxim", false)
        map.insert("names", "Yaroslav", false)
        map.insert("names", "Anton", false)
        map.insert("surname", "Mityushkin", false)
        map.insert("surname", "Evstafev", false)
        map.insert("surname", "Yankin", false)

        map.insert("year", "2002", false)
        map.insert("year", "2003", true)
        map.insert("year", "2001", false)

        val names = map.getAll("names")
        val surname = map.getAll("surname")
        val years = map.getAll("year")

        assertArrayEquals(arrayOf("Maxim", "Yaroslav", "Anton"), names!!.toArray())
        assertArrayEquals(arrayOf("Mityushkin", "Evstafev", "Yankin"), surname!!.toArray())
        assertArrayEquals(arrayOf("2003", "2002", "2001"), years!!.toArray())
    }

    @Test
    fun testRemoving() {
        val map = MultiHashMap<String, String>()

        map.insert("names", "Maxim", false)
        map.insert("names", "Yaroslav", false)
        map.insert("names", "Anton", false)
        map.insert("surname", "Mityushkin", false)
        map.insert("surname", "Evstafev", false)
        map.insert("surname", "Yankin", false)

        map.removeAll("names")
        map.removeAll("surname")

        val names = map.getAll("names")
        val surname = map.getAll("surname")

        assertNull(names)
        assertNull(surname)
    }
}