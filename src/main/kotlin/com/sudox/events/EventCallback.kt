package com.sudox.events

data class EventCallback(
        var function: (Any?) -> (Unit),
        var once: Boolean
)