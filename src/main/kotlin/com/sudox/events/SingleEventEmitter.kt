package com.sudox.events

import java.util.LinkedList

open class SingleEventEmitter {

    private val callbacks = LinkedList<EventCallback>()

    open fun emit(data: Any?) = emitInternal {
        it(data)
    }

    open fun emit(vararg data: Any?) = emitInternal {
        it(data)
    }

    private inline fun emitInternal(call: ((Any?) -> (Unit)) -> (Unit)) {
        val iterator = callbacks.iterator()

        while (iterator.hasNext()) {
            val callback = iterator.next()

            if (callback.once) {
                iterator.remove()
            }

            call(callback.function)
        }
    }

    open fun once(callback: (Any?) -> Unit) {
        callbacks.add(EventCallback(callback, true))
    }

    open fun on(callback: (Any?) -> Unit) {
        callbacks.add(EventCallback(callback, false))
    }

    open fun off(callback: (Any?) -> (Unit)) {
        val iterator = callbacks.iterator()

        while (iterator.hasNext()) {
            val current = iterator.next()

            if (current.function == callback) {
                iterator.remove()
            }
        }
    }

    open fun off() {
        callbacks.clear()
    }
}