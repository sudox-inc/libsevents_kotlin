package com.sudox.events

import com.sudox.events.structs.MultiHashMap

open class EventEmitter {

    private val callbacks = MultiHashMap<String, EventCallback>()

    open fun emit(event: String, data: Any? = null) {
        emitInternal(event) {
            it(data)
        }
    }

    open fun emit(event: String, vararg data: Any?) {
        emitInternal(event) {
            it(data)
        }
    }

    private inline fun emitInternal(event: String, call: ((Any?) -> (Unit)) -> (Unit)) {
        val eventCallbacks = callbacks.getAll(event) ?: return
        val iterator = eventCallbacks.iterator()

        while (iterator.hasNext()) {
            val callback = iterator.next()

            if (callback.once) {
                iterator.remove()
            }

            call(callback.function)
        }
    }

    open fun once(event: String, callback: (Any?) -> (Unit)) {
        callbacks.insert(event, EventCallback(callback, true), false)
    }

    open fun on(event: String, callback: (Any?) -> (Unit)) {
        callbacks.insert(event, EventCallback(callback, false), true)
    }

    open fun off(event: String, callback: (Any?) -> (Unit)) {
        val eventCallbacks = callbacks.getAll(event) ?: return
        val iterator = eventCallbacks.iterator()

        while (iterator.hasNext()) {
            val current = iterator.next()

            if (current.function == callback) {
                iterator.remove()
            }
        }
    }

    open fun off(event: String) {
        callbacks.removeAll(event)
    }
}