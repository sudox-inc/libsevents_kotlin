package com.sudox.events.structs

import java.util.LinkedList
import kotlin.collections.HashMap

class MultiHashMap<K, V> {

    private val map = HashMap<K, LinkedList<V>>()

    fun insert(key: K, value: V, first: Boolean) {
        var storage = getAll(key)

        if (storage == null) {
            storage = LinkedList()
            map[key] = storage
        }

        if (first) {
            storage.addFirst(value)
        } else {
            storage.addLast(value)
        }
    }

    fun removeAll(key: K): LinkedList<V>? {
        return map.remove(key)
    }

    fun getAll(key: K): LinkedList<V>? {
        return map[key]
    }
}